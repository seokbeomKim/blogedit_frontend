import request from 'request'

export default {
  backends: 'http://localhost:8005',

  // Blogedit backends API URLs
  categories: 'categories',
  category_groups: 'category_groups',
  users: 'users',
  platforms: 'platforms',
  posts: 'posts',

  getUrl (query) {
    return this.backends + '/' + query
  },
  getPlatformId (platformName) {
    return new Promise((resolve, reject) => {
      let platformRequestUrl = new URL(this.getUrl(this.platforms))
      let platformSearchParams = platformRequestUrl.searchParams
      platformSearchParams.append('name', platformName)

      request.get({uri: platformRequestUrl}, (error, response, result) => {
        if (error) {
          reject(error)
        } else {
          resolve(response)
        }
      })
    })
  }
}
