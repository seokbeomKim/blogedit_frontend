let popups = []

export default {
  connect: function () {
    console.log('Socket connected')
    this.$emit('sayHello', 'sayHello~!')
  },
  disconnect: function () {
    console.log('Socket disconnected')
  },
  sayHello: function () {
    console.log('Received say hello. send ACK...')
    this.$emit('replyHello')
  },
  TISTORY_AUTH: (data) => {
    var child = window.open(data, '_blank', 'nodeIntegration=no')
    popups.unshift(child)
  },
  TISTORY_ACCESS_CODE: (data) => {
    window.open(data, '_blank', 'nodeIntegration=no')
  },
  CLOSE_POPUP: (data) => {
    if (popups) {
      var targetWindow = popups.pop()
      targetWindow.close()
    }
  }
}
