// configuratino loader
import fs from 'fs'
import config from './app.config'

export default {
  configPath: 'config',
  saveConfig: () => {
    console.log('Save a configuration file')
    try {
      fs.writeFileSync('config.json', JSON.stringify(config), 'utf-8')
    } catch (e) {
      alert('Failed to save the file !')
    }
  },
  /**
   * config.json 파일 로드 후 설정파일 내용을 JSON 형태로 반환한다.
   * 만약 파일 로드가 실패할 경우(설정파일 부재) 기본 설정값(app.config.js)을 사용한다.
   */
  loadConfig: function () {
    console.log('Load a configuration file, config.json')
    try {
      let read = JSON.parse(fs.readFileSync('config.json', 'utf-8'))
      config.languages = read.languages
      config.currentLang = read.currentLang
      config.fallbackLng = read.fallbackLng
      config.namespace = read.namespace
      config.platform = read.platform
      return read
    } catch (e) {
      console.error(e)
    }
  },
  getConfig: function () {
    return config
  }
}
