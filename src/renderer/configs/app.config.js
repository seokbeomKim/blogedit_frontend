export default {
  platform: process.platform,
  languages: ['ko', 'en'],
  fallbackLng: 'en',
  currentLang: 'ko',
  namespace: 'translation'
}
