// i18next.config.js
// Application language settings

import i18n from 'i18next'
import i18nextBackend from 'i18next-node-fs-backend'
import config from './app.config'
import path from 'path'

const i18nextOptions = {
  backend: {
    loadPath: path.join(__dirname, 'locales/{{lng}}/{{ns}}.json'),
    jsonIndent: 2
  },
  saveMissing: true,
  fallbackLng: config.fallbackLng,
  whitelist: config.languages
}

if (!i18n.isInitialized) {
  i18n.use(i18nextBackend).init(i18nextOptions, (err) => {
    if (err) {
      console.error('Failed to init 18next: ', err)
    }
    i18n.changeLanguage(config.currentLang)
  })
}

export default i18n
