import Vue from 'vue'
import Router from 'vue-router'

import Editor from '../components/Editor'
import PostReady from '../components/postReady'
import Setting from '../components/Setting'
import Timemachine from '../components/Timemachine'
import Notebook from '../components/Notebook'

import Language from '../components/settings/language'
import Appearance from '../components/settings/appearance'
import AccountPage from '../components/settings/accountPage'
import KeyBindings from '../components/settings/keybindings'

import TmBackup from '../components/timemachine/backup'
import TmRestore from '../components/timemachine/restore'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Editor
    },
    {
      path: '/setting',
      component: Setting,
      children: [
        {
          path: 'language',
          component: Language
        },
        {
          path: 'appearance',
          component: Appearance
        },
        {
          path: 'account_page',
          component: AccountPage
        },
        {
          path: 'keybindings',
          component: KeyBindings
        },
        {
          path: '*',
          redirect: '/setting/account_page' // children main
        }
      ]
    },
    {
      path: '/editor',
      component: Editor
    },
    {
      path: '/notebook',
      component: Notebook
    },
    {
      path: '/post_ready',
      component: PostReady
    },
    {
      path: '/timemachine',
      component: Timemachine,
      children: [
        {
          path: 'backup',
          component: TmBackup
        },
        {
          path: 'restore',
          component: TmRestore
        },
        {
          path: '*',
          redirect: '/timemachine/backup'
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
