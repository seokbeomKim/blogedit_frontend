// blogedit 에서 임시적으로 사용하는 내용들 (사용자가 작성한 초안 등의 내용을 임시로 저장하기 위한 single ton 객체 핸들러 모듈)
let tempData = []

export default {
  saveData: (key, value) => {
    tempData[key] = value
  },
  getData: (key) => {
    return tempData[key]
  }
}
