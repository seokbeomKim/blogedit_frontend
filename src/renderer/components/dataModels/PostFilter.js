// PostFilter.js
// Notebook vue instance 에서 사용자가 사용할 포스팅에 대한 필터를 추가할 때 사용하는
// 필터 객체이다.
'use strict'

export default class PostFilter {
  /**
   * filterName 으로 포스팅 필터 객체를 초기화한다.
   * @param  {String} filterName
   */
  constructor (filterName) {
    this.filterName = filterName
    this.condition = 'AND'
    this.operands = []
  }

  toggleCondition () {
    if (this.condition === 'AND') {
      this.condition = 'OR'
    } else {
      this.condition = 'AND'
    }
  }
}
